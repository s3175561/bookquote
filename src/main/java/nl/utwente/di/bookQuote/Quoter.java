package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> BookPrices;
    public Quoter (){
        BookPrices = new HashMap<>();
        BookPrices.put("1", 10.0);
        BookPrices.put("2", 45.0);
        BookPrices.put("3", 20.0);
        BookPrices.put("4", 35.0);
        BookPrices.put("5", 50.0);
        BookPrices.put("others", 0.0);
    }
    public double getBookPrice  (String isbn){
        return BookPrices.get(isbn);
    }
}
